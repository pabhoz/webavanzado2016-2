<!DOCTYPE html>
<html lang="es" >
  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" href="./assets/favicon.ico">

	    <title>CRUD</title>

	    <!-- Bootstrap core CSS -->
	    <link href="../public/css/bootstrap.min.css" rel="stylesheet">

	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <link href="../public/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="../public/css/navbar-fixed-top.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	    <script src="../public/js/ie-emulation-modes-warning.js"></script>

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="../public/css/style.css">
	    <script type="text/javascript" src="../public/js/angular.min.js"></script>
		<script src="../public/js/angular-route.min.js"></script>
		<script type="text/javascript" src="../public/controllers/app.js"></script>
	    <script type="text/javascript" src="../public/controllers/controller.js"></script>
  </head>

  <body >

	    <div class="container">
	    	
			
		    <!-- Fixed navbar -->
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		      <div class="container">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand" href="#">CRUD</a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav">
		            <li class="active"><a href="listaUsuarios.php">Home</a></li>		            
		          </ul>
		          <ul class="nav navbar-nav navbar-right" >
		            <li class="dropdown">
		              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Bienvenido  <span class="caret"></span></a>
		              <ul class="dropdown-menu">
		                <li><a href="./views/login.html">Salir <span class="glyphicon glyphicon-log-out"></span></a></li>	                
		              </ul>
		            </li>
		            
		          </ul>
		        </div><!--/.nav-collapse -->
		      </div>
		    </nav>

	    	<div class="container">			
				<div class="panel panel-default">
					<div class="panel-heading"><strong> Crear Usuario</strong></div>
				  	<div class="panel-body">
				    	<form method="post" action="create.php">			    		
				    		<div class="form-group">
							    <label for="txtUsername">Nickname</label>
							    <input type="text" class="form-control" id="txtUsername" name="username" placeholder="Nickname">
							</div>
							<div class="form-group">
							    <label for="txtName">Nombre</label>
							    <input type="text" class="form-control" id="txtName" name="name" placeholder="Nombre">
							</div>
				    		<div class="form-group">
							    <label for="txtEmail">Correo</label>
							    <input type="email" class="form-control" id="txtEmail" name="email" placeholder="Correo">
							</div>
							<div class="form-group">
							    <label for="txtPassword">Contraseña</label>
							    <input type="password" class="form-control" id="txtPassword" name="password" placeholder="Contraseña">
							</div>
							<div class="" align="center">
								<button class="btn btn-success" type="submit">Guardar</button>					
								<a class="btn btn-danger" href="listaUsuarios.php">Regresar</a>
								
							</div>
				    	</form>
				  	</div>				
					

				</div>
				
	    	</div> <!-- /container -->


	    </div>
	    <script src="../public/js/jquery-2.2.1.min.js"></script>   
	    <script src="../public/js/bootstrap.min.js"></script>   
  </body>
</html>

<?php 

	include 'lazyLoad.php';


	if($_POST){
		$username = $_POST['username'];
		$name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
                
                
        $user = new User();
        $user->setName($name);
        $user->setUsername($username);
        $user->setPassword($password);
        $user->setEmail($email);
        
        $vars = $user->getMyVars();
        User::save($vars);
        
                 
                
	}
	
 ?>