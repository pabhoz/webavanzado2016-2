<?php

class File {
    
    public static function upload($file,$target_dir){

        $target_file = $target_dir ."/" . basename($file["name"]);
        
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            return $target_file;            
        } else {
            return false;
        }
    }
    
}
