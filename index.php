<!DOCTYPE html>
<html lang="es" >
	<?php
		include 'lazyLoad.php';
	?>
  <head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" href="./assets/favicon.ico">

	    <title>CRUD</title>

	    <!-- Bootstrap core CSS -->
	    <link href="./public/css/bootstrap.min.css" rel="stylesheet">

	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <link href="./public/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="./public/css/navbar-fixed-top.css" rel="stylesheet">

	    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	    <script src="./public/js/ie-emulation-modes-warning.js"></script>

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="./public/css/style.css">
	    <script type="text/javascript" src="./public/js/angular.min.js"></script>
  </head>

  <body >

	    <div class="container">


		    <!-- Fixed navbar -->
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		      <div class="container">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand" href="#">CRUD</a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav">
		            <li class="active"><a href="listaUsuarios.php">Home</a></li>
		          </ul>
		          <ul class="nav navbar-nav navbar-right" >
		            <li class="dropdown">
		              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Bienvenido  <span class="caret"></span></a>
		              <ul class="dropdown-menu">
		                <li><a href="./views/login.html">Salir <span class="glyphicon glyphicon-log-out"></span></a></li>
		                <li><a href="./views/login.html">No salir <span class="glyphicon glyphicon-log-out"></span></a></li>
		              </ul>
		            </li>

		          </ul>
		        </div><!--/.nav-collapse -->
		      </div>
		    </nav>

	    	<div class="container">
				<div class="jumbotron">
					<div class="panel panel-default" align="">
						<a class="btn btn-success" href="create.php">Crear Usuario</a>
					</div>

					<div class="table-responsive">
						<table class="table table-hover table-bordered table-responsive">
							<thead>
								<tr>
							    	<th>id</th>
							    	<th>nombre</th>
							    	<th>Nick</th>
							    	<th>Correo</th>
							    	<th>contraseña</th>
							    	<th>Opciones</th>
							  	</tr>
							</thead>
							<tbody>
								<?php function enmascarar($text){
										$mascara = "";
										$n = strlen($text);
										while($n > 0){
											$mascara.="*";
											$n--;
										}
										return $mascara;
								}
								?>
								<?php $usuarios = User::selectAll(); ?>
								<?php if(!empty($usuarios)): ?>
								<?php foreach($usuarios as $usuario): ?>

												<tr>
						            <td> <?php echo $usuario["id"];?></td>
								    		<td><?php echo $usuario["name"];?></td>
								    		<td><?php echo $usuario["username"];?></td>
								    		<td><?php echo $usuario["email"];?></td>
								    		<td><?php echo enmascarar($usuario["password"]);?></td>
								    		<td>
								    			<?php echo "<a href='update.php?codigo= ".$usuario["id"]."' class='btn btn-primary'>"; ?>
												    <span class="glyphicon glyphicon-pencil"></span>
												</a>
									    			<button class="btn btn-danger" onclick="deleteUser(<?php echo $usuario["id"]; ?>)">
													    <span class="glyphicon glyphicon-remove"></span>
													</button>


								    		</td>
							    		<tr>
							<?php endforeach; ?>
						<? endif; ?>
							</tbody>
						</table>
					</div>
				</div>
	    	</div> <!-- /container -->


	    </div>
	    <script src="./public/js/jquery-2.2.1.min.js"></script>
	    <script src="./public/js/bootstrap.min.js"></script>

	    <script type="text/javascript">

			function deleteUser( id ){

			    var answer = confirm('Are you sure?');
			    if (answer){
			        // if user clicked ok,
			        // pass the id to delete.php and execute the delete query
			        window.location = 'delete.php?id=' + id;
			    }
			}

	    </script>
  </body>
</html>
