<?php

spl_autoload_register(function ($clase) {
    if(file_exists('./Modelo/' . $clase . '.php')){
        include './Modelo/' . $clase . '.php';
    }elseif(file_exists('./libraries/' . $clase . '.php')){
        include './libraries/' . $clase . '.php';
    }else{
        exit("Clase ".$class." no encontrada");
    }
});
